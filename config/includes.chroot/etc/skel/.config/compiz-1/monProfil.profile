[cubeaddon]
s0_deformation = 0

[move]
s0_opacity = 74

[core]
s0_active_plugins = core;composite;opengl;decor;matecompat;resize;move;text;mousepoll;regex;wobbly;ring;workspacenames;animation;cube;td;showmouse;rotate;cubeaddon;
s0_hsize = 4

[animation]
s0_minimize_effects = animation:Magic Lamp Wavy;
s0_minimize_durations = 250;
s0_minimize_matches = (type=Normal | Dialog | ModalDialog | Unknown);
s0_minimize_options = ;
s0_minimize_random_effects = 

[cube]
s0_skydome = true
s0_active_opacity = 80.000000

[ring]
s0_next_key = <Alt>Tab

